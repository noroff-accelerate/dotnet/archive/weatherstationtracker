﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ReactInteraction.Models;
using System.Net;
using System.Threading;
using System.Net.Http;


namespace ReactInteraction.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly WeatherDbContext _context;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, WeatherDbContext context)
        {
            
            _logger = logger;
            _context = context;
        }



        //demonstrates null condition and coalescing operators
        [HttpGet("/api/[controller]/ForecastSamples")]
        public IEnumerable<string> GetForecastSamples()
        {
            try
            {
                List<string> sample = new List<string>();

                var collectionSample = WeatherForecast.GetSampleForeCasts();

                foreach (var item in collectionSample)
                {
                    //uncomment and remove null instances from collection to demonstrate the extension methods for the weather forecast class
                    // sample.Add($"{item?.Id} {item?.Summary } {item?.TemperatureC } {item.GetFarenheit() } {item?.Summary } {item?.Date}");

                    //uncomment for example of nameof
                    //sample.Add($"The Id is:{item?.Id} Here is the summary {item?.Summary } the temperature is {item?.TemperatureC }");
                    //sample.Add($"The {nameof(item.Id)}:{item?.Id} is:{item?.Id} Here is the {nameof(item.Summary)} {item?.Summary } the {nameof(item.TemperatureC)} is {item?.TemperatureC }");

                    //sample.Add($"{item?.Id} {item?.Summary }{item?.TemperatureC } {item?.Summary } {item?.Date}");
                    sample.Add($"{item?.Id} {item?.Summary } {item?.TemperatureC ?? 0} {item?.Summary} {item?.Date ?? DateTime.MinValue}");

                    //sample.Add($"{item?.Id} {item?.Summary } {item?.TemperatureC } {item?.Summary } {item?.Date}");

                }
                return sample;
            }
            catch(NullReferenceException ex)
            {
              return new List<string>() {$"{ex.Message} It is likely the iterated collection contained a null object: try using null conditional operators instead"};
            }
            catch(Exception ex)
            {
                return new List<string>() { ex.Message };
            }
        }



        [HttpGet("/api/[controller]/ForecastSamplesJSON")]
        public IEnumerable<WeatherForecast> GetForecastSamplesJSON()
        {
            try
            {
                List<string> sample = new List<string>();

                var collectionSample = WeatherForecast.GetSampleForeCasts();

                foreach (var item in collectionSample)
                {
                    sample.Add($"{item?.Id ?? 0} {item?.Summary ?? "<not provided>"} {item?.TemperatureC ?? 0} {item?.Summary ?? "<not provided>"} {item?.Date ?? DateTime.MinValue}");
                }

                return collectionSample;
            }
            catch (NullReferenceException ex)
            {
                return new List<WeatherForecast>();
            }
            catch (Exception ex)
            {
                return new List<WeatherForecast>();
            }
        }

        // GET: json sample
        [HttpGet("/api/[controller]/ForecastSample")]
        public WeatherForecast GetForecastSample()
        {

            var forecasts = WeatherForecast.GetSampleForeCasts();

            return forecasts.First();

        }



        [HttpGet("/api/[controller]/WeatherStationSample")]
        public WeatherStation GetWeatherStationSample()
        {

            var weatherStation = new WeatherStation();

            return weatherStation;

        }

        [HttpPost]
        public void Post([FromBody] IEnumerable<WeatherForecast> weatherForecasts)
        {
            List<string> sample = new List<string>();

            foreach (var item in weatherForecasts)
            {
                //uncomment and remove null instances from collection to demonstrate the extension methods for the weather forecast class
                // sample.Add($"{item?.Id} {item?.Summary } {item?.TemperatureC } {item.GetFarenheit() } {item?.Summary } {item?.Date}");

                //uncomment for example of nameof
                //sample.Add($"The Id is:{item?.Id} Here is the summary {item?.Summary } the temperature is {item?.TemperatureC }");
                //sample.Add($"The {nameof(item.Id)}:{item?.Id} is:{item?.Id} Here is the {nameof(item.Summary)} {item?.Summary } the {nameof(item.TemperatureC)} is {item?.TemperatureC }");

                sample.Add($"{item?.Id} {item?.Summary }{item?.TemperatureC } {item?.Summary } {item?.Date}");
                sample.Add($"{item?.Id ?? 0} {item?.Summary ?? "<not provided>"} {item?.TemperatureC ?? 0} {item?.Summary ?? "<not provided>"} {item?.Date ?? DateTime.MinValue}");
            }


        }

        //demonstrates chaining null condition operators and coalescing and auto implemented read only
        [HttpGet("/api/[controller]/StationSamples")]
        public IEnumerable<string> StationExample()
        {
            try
            {
                bool allBuilt = true;
                List<string> sample = new List<string>();

                //demonstrates implicit conversion (var)
                foreach (var item in WeatherStation.GetRandomWeatherStations())
                {
                    sample.Add($"{item?.Id} {item?.Name} {item?.Location} {item?.Altitude} {item?.DailyForecast?.Summary}");
                    sample.Add($"{item?.Id ?? 0} {item?.Name ?? "<not provided>"} {item?.Location ?? "<not provided>"} {item?.Altitude ?? 0} {item?.DailyForecast?.Summary ?? "<no object provided>"}" );
                    
                    if(item.IsBuilt == false)
                    {
                        allBuilt = false;
                    }
                }

                if(!allBuilt)
                {
                    sample.Add("Some of these stations have not yet been built");
                }

                return sample;
            }
            catch (NullReferenceException ex)
            {
                return new List<string>() { $"{ex.Message} It is likely the iterated collection contained a null object: try using null conditional operators instead" };
            }
            catch (Exception ex)
            {
                return new List<string>() { ex.Message };
            }
        }



        //demonstrates chaining null condition operators and coalescing and auto implemented read only
        [HttpGet("/api/[controller]/ForecastFilteredSamples")]
        public IEnumerable<WeatherForecast> ForecastFilteredExample()
        {
            try
            {
                return WeatherForecast.Filter(WeatherForecast.GetSampleForeCasts(), GetEvenTemps);
                //return WeatherForecast.Filter(WeatherForecast.GetSampleForeCasts(), GetBelowZeroForecasts);
                //return WeatherForecast.Filter(WeatherForecast.GetSampleForeCasts(), wf => wf?.TemperatureC < 30);
                //return WeatherForecast.Filter(WeatherForecast.GetSampleForeCasts(), wf => wf?.TemperatureC > -30);
                //return WeatherForecast.Filter(WeatherForecast.GetSampleForeCasts(), wf => wf?.Id == 5);
            }

            catch (Exception ex)
            {
                return new List<WeatherForecast> { new WeatherForecast()};
            }
        }



        [HttpGet("/api/WebsiteData")]
        public async Task<string> GetWebsiteData()
        {
            HttpClient client = new HttpClient(); 
            
            var httpMessage = await client.GetAsync("http://www.ikea.com");

            return httpMessage.ToString();
        }

        public static bool GetEvenTemps(WeatherForecast wf)
        {
            if(wf.TemperatureC % 2 == 0)
            {
                return true;
            }
            return false;
        }

        public static bool GetBelowZeroForecasts(WeatherForecast wf)
        {
            if (wf.IsFreezing == true)
            {
                return true;
            }
            return false;
        }

    }
}
