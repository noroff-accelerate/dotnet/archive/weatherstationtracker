using System;
using System.Collections.Generic;
using System.Linq;

namespace ReactInteraction
{
    public class WeatherForecast
    {
        public int? Id { get; set; }
        public DateTime Date { get; set; }
        public int? TemperatureC { get; set; }
        public string Summary { get; set; } = "Not Provided";
        public bool IsFreezing => (TemperatureC < 0);
       
        public static IEnumerable<WeatherForecast> GetSampleForeCasts()
        {
              string[] Summaries = new[]
                 {
                     "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
                 };
            
                Random rng = new Random();
                int auto = 1;

                //demonstrates object anonymous object initializers
                List<WeatherForecast> forecasts = Enumerable.Range(1, 5).Select(index => new WeatherForecast
                {
                     Id = auto++,
                     Date = DateTime.Now.AddDays(index),
                     TemperatureC = rng.Next(-20, 55),
                     Summary = Summaries[rng.Next(Summaries.Length)]
                }).ToList();
                
                       
                forecasts.Add(new WeatherForecast() { Id = auto++, Date = DateTime.Now.AddDays(1), TemperatureC = rng.Next(-20, 55) });
                forecasts.Add(null);
                forecasts.Add(null);
                forecasts.Add(null);
                forecasts.Add(null);
                forecasts.Add(null);
                forecasts.Add(new WeatherForecast());

            

            return forecasts;
        }

        //Demonstrates index initializers 
        public static Dictionary<string, WeatherForecast> GetSampleForeCastsDictionary()
        {
            string[] Summaries = new[]
                 {
                     "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
                 };

            Random rng = new Random();
            int auto = 1;

            Dictionary<string, WeatherForecast> forecastArchives = new Dictionary<string, WeatherForecast>
            {
                ["Entry 1"] = new WeatherForecast { Id = auto++, Date = DateTime.Now.AddDays(1), Summary = Summaries[rng.Next(Summaries.Length)], TemperatureC = rng.Next(-20, 55) },
                ["Entry 2"] = new WeatherForecast { Id = auto++, Date = DateTime.Now.AddDays(1), Summary = Summaries[rng.Next(Summaries.Length)], TemperatureC = rng.Next(-20, 55) },
                ["Entry 3"] = new WeatherForecast { Id = auto++, Date = DateTime.Now.AddDays(1), Summary = Summaries[rng.Next(Summaries.Length)], TemperatureC = rng.Next(-20, 55) },
                ["Entry 4"] = new WeatherForecast { Id = auto++, Date = DateTime.Now.AddDays(1), Summary = Summaries[rng.Next(Summaries.Length)], TemperatureC = rng.Next(-20, 55) }
            };


            return forecastArchives;


        }

        public static IEnumerable<WeatherForecast> Filter(IEnumerable<WeatherForecast> forecasts, Predicate<WeatherForecast> selector)
        {
            List<WeatherForecast> result = new List<WeatherForecast>();
            
            foreach (var forecast in forecasts)
            {
                if (selector(forecast))
                {
                    result.Add(forecast);
                }
            }
            return result;


        }

    }
}
