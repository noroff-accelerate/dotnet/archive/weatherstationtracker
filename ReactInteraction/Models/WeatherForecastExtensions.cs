﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReactInteraction.Models
{
    public static class WeatherForecastExtensions
    {

        public static double? GetFarenheit(this WeatherForecast weatherForecast)
        {

            double? fTemp = (weatherForecast.TemperatureC * 1.8) + weatherForecast.TemperatureC;

            return fTemp;
            
        }

    }
}
