# Weather Station Tracker

.NET Core Web Application (React ClientApp)

Intended for learning OOP basic


## Getting Started

Clone to a local directory.

Open solution in Visual Studio

Run

Make requests to the API to see the relevant features

Front end can be ignored

Refer to in code comments for where discussed features can be seen

### Prerequisites

.NET Framework

Visual Studio 2017/19 OR Visual Studio Code


## Authors

***Dean von Schoultz** [deanvons](https://gitlab.com/deanvons)




